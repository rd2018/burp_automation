@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7' )
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.POST
import static groovyx.net.http.Method.PUT
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.TEXT
import static groovyx.net.http.ContentType.URLENC


import static groovyx.net.http.ContentType.*



def url = 'http://localhost:8090'

def host='http://192.168.249.185:8080'


def http = new HTTPBuilder(url)

def fail_build=false



//2. check if the host is in scope ,if not add it...

path = '/burp/target/scope'


http.get( path : path,
          contentType : JSON ,
          query: [url:host]){

  resp, reader ->


//3. Put a Url in Scope  

  if(!reader.inScope){

  println "Not in Scope,Adding url to scope"



  def rslt=["curl","-X", "PUT","-H", "Content-Type:application/json", "-H" ,"Accept: */*","http://localhost:8090/burp/target/scope?url=http%3A%2F%2F192.168.249.185%3A8080"].execute()
  println rslt.text


    }
}

//5. perform active scan

path = '/burp/scanner/scans/active'

http.post( path: path, body: [baseUrl:host] ,
           requestContentType: URLENC ) { resp ->

  println "POST Success: ${resp.statusLine}"

}


sleep(4000)


path='/burp/scanner/status'

complete=false
while(!complete){

http.get( path : path,
          contentType : JSON ,
          query: [url:host]){

resp, reader ->


if(reader.scanPercentage != 100){
       sleep(100000)
      // println reader.scanPercentage

  }
  else{
    complete=true


  }
}
}



//check here,if we have any Vulnerability of "High" Confidence

path='/burp/scanner/issues'


http.get( path : path,
          contentType : JSON ,
          query: [url:host]){

resp, reader ->
   ArrayList al = reader.issues;
   ListIterator litr = al.listIterator();
    while(litr.hasNext()) {
         Object element = litr.next();
         if (element['severity'] == 'High' && element['issueName']!='External service interaction (DNS)'){    //false positive 'External service interaction (DNS)'
         fail_build=true
         }


      }
}


sleep(3000)


def rslt5=["curl","-X", "GET", "-H" ,"Accept: application/octet-stream","http://localhost:8090/burp/report?reportType=HTML"].execute().text
File report=new File("report.html")
report << rslt5



sleep(3000)

//shut burp

def rslt3=["curl","-X", "GET", "-H" ,"Accept: */*","http://localhost:8090/burp/stop"].execute()

println rslt3


//Throw this,to fail build
if (fail_build){
println "Jenkins Build Failed Due to High Confidence Errors.Pls. check the report for Further Details!!!."
}

